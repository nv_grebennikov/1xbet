<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$data = [
    'бекхэм' => [
        'code' => 'apcYDzQ852g',
    ],
    'роналду' => [
        'code' => 'VAA78_4FME4',
    ],
    'роналдо' => [
        'code' => 'VAA78_4FME4',
    ],
    'престон норд энд' => [
        'code' => 'G7a0veczbj4',
    ],
    'философия' => [
        'code' => 'LVWvkPkhhBg',
    ],
    'preston' => [
        'code' => 'XLDTNMEZNag',
    ],
    '2004' => [
        'code' => '7AXkgkAVVxw',
    ],
];

$input_value = null;

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['search'])) {
    $input_value = $_POST['search'];
    $search = mb_strtolower($input_value, 'UTF-8');
    $video = isset($data[$search]) ? $data[$search] : false;  
} 

?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="ru">
    <head>
        <title>Example</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/site.css" rel="stylesheet">
    </head>
    <body>
        <div id="index" class="container-fluid">
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                    
                    <form class="form-inline text-center" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control input-lg" placeholder="Введите слово" name="search" value="<?= $input_value; ?>">
                        </div>
                        <button type="submit" class="btn btn-success btn-lg">
                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>     
                        </button>
                    </form>
                </div>
                
                <?php if ($_SERVER['REQUEST_METHOD'] === 'POST'): ?>
                    <div id="result" class="col-xs-12 text-center">
                        <?php if ($video): ?>
                                <iframe src="https://www.youtube.com/embed/<?= $video['code']; ?>?autoplay=1" frameborder="0" allowfullscreen></iframe>
                        <?php else: ?>
                            <img class="error" src="/image/cat.gif" title="Ошибка" alt="Ошибка">
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </body>
</html>
